Turn your grid into a lightbox & gallery
======

This is an extension to integrates [Magnific Popup](http://dimsemenov.com/plugins/magnific-popup/) into laravel-admin.


## Installation 

```bash
composer require xn/ext-grid-lightbox

php artisan vendor:publish --provider="Xn\Grid\Lightbox\LightboxServiceProvider" --force
```

## Configurations

Open `config/admin.php`, add configurations that belong to this extension at `extensions` section.
```php

    'extensions' => [

        'grid-lightbox' => [
        
            // Set to `false` if you want to disable this extension
            'enable' => true,
        ]
    ]

```

## Usage

Use it in the grid:
```php
// lightbox
$grid->column('avatar')->lightbox();

// gallery
$grid->column('avatar')->gallery();

// zoom
$grid->column('avatar')->lightbox(['zooming' => true]);
$grid->column('avatar')->gallery(['zooming' => true]);

// width & height
$grid->column('avatar')->lightbox(['width' => 50, 'height' => 50]);
$grid->column('avatar')->gallery(['width' => 50, 'height' => 50]);

// img class properties
$grid->column('avatar')->lightbox(['class' => 'rounded']);
$grid->column('avatar')->gallery(['class' => ['circle', 'thumbnail']]);
```

License
------------
Licensed under [The MIT License (MIT)](LICENSE).
