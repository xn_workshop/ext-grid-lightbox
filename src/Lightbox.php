<?php

namespace Xn\Grid\Lightbox;

use Xn\Admin\Extension;

class Lightbox extends Extension
{
    public $name = 'grid-lightbox';

    public $assets = __DIR__.'/../resources/assets';
}
