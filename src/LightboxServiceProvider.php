<?php

namespace Xn\Grid\Lightbox;

use Xn\Admin\Admin;
use Xn\Admin\Grid\Column;
use Illuminate\Support\ServiceProvider;

class LightboxServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function boot(Lightbox $extension)
    {
        if (! Lightbox::boot()) {
            return ;
        }

        if ($views = $extension->views()) {
            $this->loadViewsFrom($views, 'ext-grid-lightbox');
        }

        if ($this->app->runningInConsole() && $assets = $extension->assets()) {
            $this->publishes(
                [$assets => public_path('vendor/xn/ext-grid-lightbox')],
                'ext-grid-lightbox'
            );
        }

        $this->app->booted(function () {
            Admin::css('vendor/xn/ext-grid-lightbox/magnific-popup.css');
            Admin::js('vendor/xn/ext-grid-lightbox/jquery.magnific-popup.min.js');

            Column::extend('lightbox', LightboxDisplayer::class);
            Column::extend('gallery', GalleryDisplayer::class);
        });
    }
}
